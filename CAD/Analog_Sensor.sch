(kicad_sch (version 20211123) (generator eeschema)

  (uuid f8f3a9fc-1e34-4573-a767-508104e8d242)

  (paper "A4")

  (title_block
    (title "Analog IR sensor")
    (date "2022-03-11")
    (rev "v1.0")
    (comment 2 "Now consists of a red light led to indicate it is on.")
    (comment 3 "SN: KRTLUK002")
    (comment 4 "Author: Luke Kratz")
  )

  (lib_symbols
    (symbol "Device:C" (pin_numbers hide) (pin_names (offset 0.254)) (in_bom yes) (on_board yes)
      (property "Reference" "C" (id 0) (at 0.635 2.54 0)
        (effects (font (size 1.27 1.27)) (justify left))
      )
      (property "Value" "C" (id 1) (at 0.635 -2.54 0)
        (effects (font (size 1.27 1.27)) (justify left))
      )
      (property "Footprint" "" (id 2) (at 0.9652 -3.81 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "Datasheet" "~" (id 3) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "ki_keywords" "cap capacitor" (id 4) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "ki_description" "Unpolarized capacitor" (id 5) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "ki_fp_filters" "C_*" (id 6) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (symbol "C_0_1"
        (polyline
          (pts
            (xy -2.032 -0.762)
            (xy 2.032 -0.762)
          )
          (stroke (width 0.508) (type default) (color 0 0 0 0))
          (fill (type none))
        )
        (polyline
          (pts
            (xy -2.032 0.762)
            (xy 2.032 0.762)
          )
          (stroke (width 0.508) (type default) (color 0 0 0 0))
          (fill (type none))
        )
      )
      (symbol "C_1_1"
        (pin passive line (at 0 3.81 270) (length 2.794)
          (name "~" (effects (font (size 1.27 1.27))))
          (number "1" (effects (font (size 1.27 1.27))))
        )
        (pin passive line (at 0 -3.81 90) (length 2.794)
          (name "~" (effects (font (size 1.27 1.27))))
          (number "2" (effects (font (size 1.27 1.27))))
        )
      )
    )
    (symbol "Device:LED" (pin_numbers hide) (pin_names (offset 1.016) hide) (in_bom yes) (on_board yes)
      (property "Reference" "D" (id 0) (at 0 2.54 0)
        (effects (font (size 1.27 1.27)))
      )
      (property "Value" "LED" (id 1) (at 0 -2.54 0)
        (effects (font (size 1.27 1.27)))
      )
      (property "Footprint" "" (id 2) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "Datasheet" "~" (id 3) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "ki_keywords" "LED diode" (id 4) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "ki_description" "Light emitting diode" (id 5) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "ki_fp_filters" "LED* LED_SMD:* LED_THT:*" (id 6) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (symbol "LED_0_1"
        (polyline
          (pts
            (xy -1.27 -1.27)
            (xy -1.27 1.27)
          )
          (stroke (width 0.254) (type default) (color 0 0 0 0))
          (fill (type none))
        )
        (polyline
          (pts
            (xy -1.27 0)
            (xy 1.27 0)
          )
          (stroke (width 0) (type default) (color 0 0 0 0))
          (fill (type none))
        )
        (polyline
          (pts
            (xy 1.27 -1.27)
            (xy 1.27 1.27)
            (xy -1.27 0)
            (xy 1.27 -1.27)
          )
          (stroke (width 0.254) (type default) (color 0 0 0 0))
          (fill (type none))
        )
        (polyline
          (pts
            (xy -3.048 -0.762)
            (xy -4.572 -2.286)
            (xy -3.81 -2.286)
            (xy -4.572 -2.286)
            (xy -4.572 -1.524)
          )
          (stroke (width 0) (type default) (color 0 0 0 0))
          (fill (type none))
        )
        (polyline
          (pts
            (xy -1.778 -0.762)
            (xy -3.302 -2.286)
            (xy -2.54 -2.286)
            (xy -3.302 -2.286)
            (xy -3.302 -1.524)
          )
          (stroke (width 0) (type default) (color 0 0 0 0))
          (fill (type none))
        )
      )
      (symbol "LED_1_1"
        (pin passive line (at -3.81 0 0) (length 2.54)
          (name "K" (effects (font (size 1.27 1.27))))
          (number "1" (effects (font (size 1.27 1.27))))
        )
        (pin passive line (at 3.81 0 180) (length 2.54)
          (name "A" (effects (font (size 1.27 1.27))))
          (number "2" (effects (font (size 1.27 1.27))))
        )
      )
    )
    (symbol "Device:R" (pin_numbers hide) (pin_names (offset 0)) (in_bom yes) (on_board yes)
      (property "Reference" "R" (id 0) (at 2.032 0 90)
        (effects (font (size 1.27 1.27)))
      )
      (property "Value" "R" (id 1) (at 0 0 90)
        (effects (font (size 1.27 1.27)))
      )
      (property "Footprint" "" (id 2) (at -1.778 0 90)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "Datasheet" "~" (id 3) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "ki_keywords" "R res resistor" (id 4) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "ki_description" "Resistor" (id 5) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "ki_fp_filters" "R_*" (id 6) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (symbol "R_0_1"
        (rectangle (start -1.016 -2.54) (end 1.016 2.54)
          (stroke (width 0.254) (type default) (color 0 0 0 0))
          (fill (type none))
        )
      )
      (symbol "R_1_1"
        (pin passive line (at 0 3.81 270) (length 1.27)
          (name "~" (effects (font (size 1.27 1.27))))
          (number "1" (effects (font (size 1.27 1.27))))
        )
        (pin passive line (at 0 -3.81 90) (length 1.27)
          (name "~" (effects (font (size 1.27 1.27))))
          (number "2" (effects (font (size 1.27 1.27))))
        )
      )
    )
    (symbol "Sensor_Temperature:MCP9700AT-ETT" (pin_names (offset 1.016)) (in_bom yes) (on_board yes)
      (property "Reference" "U" (id 0) (at -6.35 6.35 0)
        (effects (font (size 1.27 1.27)))
      )
      (property "Value" "MCP9700AT-ETT" (id 1) (at 1.27 6.35 0)
        (effects (font (size 1.27 1.27)) (justify left))
      )
      (property "Footprint" "Package_TO_SOT_SMD:SOT-23" (id 2) (at 0 -10.16 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "Datasheet" "http://ww1.microchip.com/downloads/en/DeviceDoc/21942e.pdf" (id 3) (at -3.81 6.35 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "ki_keywords" "temperature sensor thermistor" (id 4) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "ki_description" "Low power, analog thermistor temperature sensor, ±2C accuracy, -40C to +125C, in SOT-23-3" (id 5) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "ki_fp_filters" "SOT?23*" (id 6) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (symbol "MCP9700AT-ETT_0_1"
        (rectangle (start -7.62 5.08) (end 7.62 -5.08)
          (stroke (width 0.254) (type default) (color 0 0 0 0))
          (fill (type background))
        )
        (circle (center -4.445 -2.54) (radius 1.27)
          (stroke (width 0.254) (type default) (color 0 0 0 0))
          (fill (type outline))
        )
        (rectangle (start -3.81 -1.905) (end -5.08 0)
          (stroke (width 0.254) (type default) (color 0 0 0 0))
          (fill (type outline))
        )
        (arc (start -3.81 3.175) (mid -4.445 3.81) (end -5.08 3.175)
          (stroke (width 0.254) (type default) (color 0 0 0 0))
          (fill (type none))
        )
        (polyline
          (pts
            (xy -5.08 0.635)
            (xy -4.445 0.635)
          )
          (stroke (width 0.254) (type default) (color 0 0 0 0))
          (fill (type none))
        )
        (polyline
          (pts
            (xy -5.08 1.27)
            (xy -4.445 1.27)
          )
          (stroke (width 0.254) (type default) (color 0 0 0 0))
          (fill (type none))
        )
        (polyline
          (pts
            (xy -5.08 1.905)
            (xy -4.445 1.905)
          )
          (stroke (width 0.254) (type default) (color 0 0 0 0))
          (fill (type none))
        )
        (polyline
          (pts
            (xy -5.08 2.54)
            (xy -4.445 2.54)
          )
          (stroke (width 0.254) (type default) (color 0 0 0 0))
          (fill (type none))
        )
        (polyline
          (pts
            (xy -5.08 3.175)
            (xy -5.08 0)
          )
          (stroke (width 0.254) (type default) (color 0 0 0 0))
          (fill (type none))
        )
        (polyline
          (pts
            (xy -5.08 3.175)
            (xy -4.445 3.175)
          )
          (stroke (width 0.254) (type default) (color 0 0 0 0))
          (fill (type none))
        )
        (polyline
          (pts
            (xy -3.81 3.175)
            (xy -3.81 0)
          )
          (stroke (width 0.254) (type default) (color 0 0 0 0))
          (fill (type none))
        )
      )
      (symbol "MCP9700AT-ETT_1_1"
        (pin power_in line (at 0 7.62 270) (length 2.54)
          (name "Vdd" (effects (font (size 1.27 1.27))))
          (number "1" (effects (font (size 1.27 1.27))))
        )
        (pin output line (at 10.16 0 180) (length 2.54)
          (name "Vout" (effects (font (size 1.27 1.27))))
          (number "2" (effects (font (size 1.27 1.27))))
        )
        (pin power_in line (at 0 -7.62 90) (length 2.54)
          (name "GND" (effects (font (size 1.27 1.27))))
          (number "3" (effects (font (size 1.27 1.27))))
        )
      )
    )
    (symbol "power:+2V8" (power) (pin_names (offset 0)) (in_bom yes) (on_board yes)
      (property "Reference" "#PWR" (id 0) (at 0 -3.81 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "Value" "+2V8" (id 1) (at 0 3.556 0)
        (effects (font (size 1.27 1.27)))
      )
      (property "Footprint" "" (id 2) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "Datasheet" "" (id 3) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "ki_keywords" "power-flag" (id 4) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "ki_description" "Power symbol creates a global label with name \"+2V8\"" (id 5) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (symbol "+2V8_0_1"
        (polyline
          (pts
            (xy -0.762 1.27)
            (xy 0 2.54)
          )
          (stroke (width 0) (type default) (color 0 0 0 0))
          (fill (type none))
        )
        (polyline
          (pts
            (xy 0 0)
            (xy 0 2.54)
          )
          (stroke (width 0) (type default) (color 0 0 0 0))
          (fill (type none))
        )
        (polyline
          (pts
            (xy 0 2.54)
            (xy 0.762 1.27)
          )
          (stroke (width 0) (type default) (color 0 0 0 0))
          (fill (type none))
        )
      )
      (symbol "+2V8_1_1"
        (pin power_in line (at 0 0 90) (length 0) hide
          (name "+2V8" (effects (font (size 1.27 1.27))))
          (number "1" (effects (font (size 1.27 1.27))))
        )
      )
    )
    (symbol "power:GND" (power) (pin_names (offset 0)) (in_bom yes) (on_board yes)
      (property "Reference" "#PWR" (id 0) (at 0 -6.35 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "Value" "GND" (id 1) (at 0 -3.81 0)
        (effects (font (size 1.27 1.27)))
      )
      (property "Footprint" "" (id 2) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "Datasheet" "" (id 3) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "ki_keywords" "power-flag" (id 4) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (property "ki_description" "Power symbol creates a global label with name \"GND\" , ground" (id 5) (at 0 0 0)
        (effects (font (size 1.27 1.27)) hide)
      )
      (symbol "GND_0_1"
        (polyline
          (pts
            (xy 0 0)
            (xy 0 -1.27)
            (xy 1.27 -1.27)
            (xy 0 -2.54)
            (xy -1.27 -1.27)
            (xy 0 -1.27)
          )
          (stroke (width 0) (type default) (color 0 0 0 0))
          (fill (type none))
        )
      )
      (symbol "GND_1_1"
        (pin power_in line (at 0 0 270) (length 0) hide
          (name "GND" (effects (font (size 1.27 1.27))))
          (number "1" (effects (font (size 1.27 1.27))))
        )
      )
    )
  )

  (junction (at 240.03 112.395) (diameter 0) (color 0 0 0 0)
    (uuid 3f8a5430-68a9-4732-9b89-4e00dd8ae219)
  )
  (junction (at 179.07 90.17) (diameter 0) (color 0 0 0 0)
    (uuid 7f202b5d-3822-42f2-966e-484d248f7525)
  )
  (junction (at 210.82 76.2) (diameter 0) (color 0 0 0 0)
    (uuid 8e980cef-5781-495e-a6e1-26b895a05477)
  )

  (wire (pts (xy 179.07 90.17) (xy 179.07 104.775))
    (stroke (width 0) (type default) (color 0 0 0 0))
    (uuid 0ad46188-7add-45cc-8f86-202997e868d0)
  )
  (wire (pts (xy 240.665 76.2) (xy 240.665 81.28))
    (stroke (width 0) (type default) (color 0 0 0 0))
    (uuid 2035ea48-3ef5-4d7f-8c3c-50981b30c89a)
  )
  (wire (pts (xy 210.82 90.17) (xy 210.82 76.2))
    (stroke (width 0) (type default) (color 0 0 0 0))
    (uuid 29bb7297-26fb-4776-9266-2355d022bab0)
  )
  (wire (pts (xy 163.195 90.17) (xy 179.07 90.17))
    (stroke (width 0) (type default) (color 0 0 0 0))
    (uuid 30c33e3e-fb78-498d-bffe-76273d527004)
  )
  (wire (pts (xy 179.07 90.17) (xy 210.82 90.17))
    (stroke (width 0) (type default) (color 0 0 0 0))
    (uuid 33cf1533-e184-41b3-824e-0faa19aea0bb)
  )
  (wire (pts (xy 163.195 104.775) (xy 163.195 102.87))
    (stroke (width 0) (type default) (color 0 0 0 0))
    (uuid 35c78346-d6ee-4c4d-81d4-33394c9398a8)
  )
  (wire (pts (xy 210.82 127.635) (xy 210.82 139.065))
    (stroke (width 0) (type default) (color 0 0 0 0))
    (uuid 36d783e7-096f-4c97-9672-7e08c083b87b)
  )
  (wire (pts (xy 254 112.395) (xy 240.03 112.395))
    (stroke (width 0) (type default) (color 0 0 0 0))
    (uuid 42ff012d-5eb7-42b9-bb45-415cf26799c6)
  )
  (wire (pts (xy 189.23 112.395) (xy 240.03 112.395))
    (stroke (width 0) (type default) (color 0 0 0 0))
    (uuid 4c843bdb-6c9e-40dd-85e2-0567846e18ba)
  )
  (wire (pts (xy 240.03 142.875) (xy 240.03 130.175))
    (stroke (width 0) (type default) (color 0 0 0 0))
    (uuid 6ffdf05e-e119-49f9-85e9-13e4901df42a)
  )
  (wire (pts (xy 240.03 122.555) (xy 240.03 112.395))
    (stroke (width 0) (type default) (color 0 0 0 0))
    (uuid 72b36951-3ec7-4569-9c88-cf9b4afe1cae)
  )
  (wire (pts (xy 240.665 100.33) (xy 240.665 105.41))
    (stroke (width 0) (type default) (color 0 0 0 0))
    (uuid 7a2f50f6-0c99-4e8d-9c2a-8f2f961d2e6d)
  )
  (wire (pts (xy 163.195 95.25) (xy 163.195 90.17))
    (stroke (width 0) (type default) (color 0 0 0 0))
    (uuid 7b87a984-8906-4efb-a98b-2df5de88605c)
  )
  (wire (pts (xy 210.82 76.2) (xy 240.665 76.2))
    (stroke (width 0) (type default) (color 0 0 0 0))
    (uuid 7f4a9ce9-8eb9-4bb2-8685-023d925acb70)
  )
  (wire (pts (xy 210.82 69.85) (xy 210.82 76.2))
    (stroke (width 0) (type default) (color 0 0 0 0))
    (uuid 80a357b5-7171-483e-a4f2-38c8e8a155d7)
  )
  (wire (pts (xy 240.665 88.9) (xy 240.665 92.71))
    (stroke (width 0) (type default) (color 0 0 0 0))
    (uuid ae0e6b31-27d7-4383-a4fc-7557b0a19382)
  )
  (wire (pts (xy 179.07 120.015) (xy 179.07 127.635))
    (stroke (width 0) (type default) (color 0 0 0 0))
    (uuid c9b9e62d-dede-4d1a-9a05-275614f8bdb2)
  )
  (wire (pts (xy 179.07 127.635) (xy 210.82 127.635))
    (stroke (width 0) (type default) (color 0 0 0 0))
    (uuid e5217a0c-7f55-4c30-adda-7f8d95709d1b)
  )

  (global_label "PB1" (shape input) (at 254 112.395 0) (fields_autoplaced)
    (effects (font (size 1.27 1.27)) (justify left))
    (uuid c3b3d7f4-943f-4cff-b180-87ef3e1bcbff)
    (property "Intersheet References" "${INTERSHEET_REFS}" (id 0) (at 358.14 206.375 0)
      (effects (font (size 1.27 1.27)) hide)
    )
  )

  (symbol (lib_id "Device:C") (at 163.195 99.06 0) (unit 1)
    (in_bom yes) (on_board yes)
    (uuid 00000000-0000-0000-0000-0000622aade4)
    (property "Reference" "C3" (id 0) (at 166.116 97.8916 0)
      (effects (font (size 1.27 1.27)) (justify left))
    )
    (property "Value" "10uF" (id 1) (at 166.116 100.203 0)
      (effects (font (size 1.27 1.27)) (justify left))
    )
    (property "Footprint" "Capacitor_SMD:C_0201_0603Metric" (id 2) (at 164.1602 102.87 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Datasheet" "https://datasheet.lcsc.com/lcsc/1811021421_Samsung-Electro-Mechanics-CL10A106MP8NNNC_C85713.pdf" (id 3) (at 163.195 99.06 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Price" "0,13" (id 4) (at 163.195 99.06 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (pin "1" (uuid 4396628c-3340-489f-9b24-ea91d30691c9))
    (pin "2" (uuid a787de0b-7ccc-4147-b23c-b19cab72577e))
  )

  (symbol (lib_id "Device:R") (at 240.03 126.365 180) (unit 1)
    (in_bom yes) (on_board yes)
    (uuid 00000000-0000-0000-0000-0000622ab9aa)
    (property "Reference" "R5" (id 0) (at 238.252 127.5334 0)
      (effects (font (size 1.27 1.27)) (justify left))
    )
    (property "Value" "10K" (id 1) (at 238.252 125.222 0)
      (effects (font (size 1.27 1.27)) (justify left))
    )
    (property "Footprint" "Resistor_SMD:R_0603_1608Metric" (id 2) (at 241.808 126.365 90)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Datasheet" "https://datasheet.lcsc.com/lcsc/1811081617_YAGEO-RC0603JR-0710KL_C99198.pdf" (id 3) (at 240.03 126.365 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Price" "0,06" (id 4) (at 240.03 126.365 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (pin "1" (uuid 9719c8a9-2c4a-44c0-b315-f4836b3e2791))
    (pin "2" (uuid 0a6a73a9-bfc6-4e61-874c-06d2ff5eee59))
  )

  (symbol (lib_id "power:GND") (at 210.82 139.065 0) (unit 1)
    (in_bom yes) (on_board yes)
    (uuid 00000000-0000-0000-0000-0000622abd22)
    (property "Reference" "#PWR06" (id 0) (at 210.82 145.415 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Value" "GND" (id 1) (at 210.947 143.4592 0))
    (property "Footprint" "" (id 2) (at 210.82 139.065 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Datasheet" "" (id 3) (at 210.82 139.065 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (pin "1" (uuid d969837d-5815-4858-bcea-0697ab8a356c))
  )

  (symbol (lib_id "Device:LED") (at 240.665 96.52 90) (unit 1)
    (in_bom yes) (on_board yes)
    (uuid 00000000-0000-0000-0000-0000622b38fd)
    (property "Reference" "D2" (id 0) (at 243.6368 95.5294 90)
      (effects (font (size 1.27 1.27)) (justify right))
    )
    (property "Value" "LED" (id 1) (at 243.6368 97.8408 90)
      (effects (font (size 1.27 1.27)) (justify right))
    )
    (property "Footprint" "LED_SMD:LED_0805_2012Metric" (id 2) (at 240.665 96.52 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Datasheet" "https://datasheet.lcsc.com/lcsc/1810271707_Everlight-Elec-17-21SYGC-S530-E3-TR8_C142303.pdf" (id 3) (at 240.665 96.52 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Price" "0,2" (id 4) (at 240.665 96.52 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (pin "1" (uuid 2333adec-ead7-4e2b-8fee-df28bec178e5))
    (pin "2" (uuid 1d968e22-8dd8-4fac-a007-c642fea56cbe))
  )

  (symbol (lib_id "Device:R") (at 240.665 85.09 0) (unit 1)
    (in_bom yes) (on_board yes)
    (uuid 00000000-0000-0000-0000-0000622b3ea1)
    (property "Reference" "R8" (id 0) (at 242.443 83.9216 0)
      (effects (font (size 1.27 1.27)) (justify left))
    )
    (property "Value" "10K" (id 1) (at 242.443 86.233 0)
      (effects (font (size 1.27 1.27)) (justify left))
    )
    (property "Footprint" "Resistor_SMD:R_0603_1608Metric" (id 2) (at 238.887 85.09 90)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Datasheet" "https://datasheet.lcsc.com/lcsc/1811081617_YAGEO-RC0603JR-0710KL_C99198.pdf" (id 3) (at 240.665 85.09 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Price" "0,06" (id 4) (at 240.665 85.09 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (pin "1" (uuid c178c866-cd3f-4465-a166-cd581aa9f014))
    (pin "2" (uuid cac5dd4d-377b-499d-832d-6cc085238b14))
  )

  (symbol (lib_id "power:GND") (at 240.665 105.41 0) (unit 1)
    (in_bom yes) (on_board yes)
    (uuid 00000000-0000-0000-0000-0000622b4332)
    (property "Reference" "#PWR018" (id 0) (at 240.665 111.76 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Value" "GND" (id 1) (at 240.792 109.8042 0))
    (property "Footprint" "" (id 2) (at 240.665 105.41 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Datasheet" "" (id 3) (at 240.665 105.41 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (pin "1" (uuid 97e59e1c-e5d2-4d05-a9fb-968536188e0f))
  )

  (symbol (lib_id "power:+2V8") (at 240.03 142.875 180) (unit 1)
    (in_bom yes) (on_board yes) (fields_autoplaced)
    (uuid 41aad57c-9f74-45cf-982b-ff1ed8e86bdf)
    (property "Reference" "#PWR?" (id 0) (at 240.03 139.065 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Value" "+2V8" (id 1) (at 240.03 147.955 0))
    (property "Footprint" "" (id 2) (at 240.03 142.875 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Datasheet" "" (id 3) (at 240.03 142.875 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (pin "1" (uuid 4fa6f24d-6af6-4e6e-94cb-95aa9bc82ff8))
  )

  (symbol (lib_id "Sensor_Temperature:MCP9700AT-ETT") (at 179.07 112.395 0) (unit 1)
    (in_bom yes) (on_board yes) (fields_autoplaced)
    (uuid 4d843028-f08c-4cdd-bb75-fda5d2ff9e3b)
    (property "Reference" "U2" (id 0) (at 169.545 111.1249 0)
      (effects (font (size 1.27 1.27)) (justify right))
    )
    (property "Value" "MCP9700AT-ETT" (id 1) (at 169.545 113.6649 0)
      (effects (font (size 1.27 1.27)) (justify right))
    )
    (property "Footprint" "Package_TO_SOT_SMD:SOT-23" (id 2) (at 179.07 122.555 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Datasheet" "http://ww1.microchip.com/downloads/en/DeviceDoc/21942e.pdf" (id 3) (at 175.26 106.045 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (pin "1" (uuid a9df5342-ce95-4f93-b7be-e8ef7969f308))
    (pin "2" (uuid b31b70e1-3e60-4a02-ad4b-ae44d37a7244))
    (pin "3" (uuid 5012d35e-3af9-4e53-b856-1b235e1f2af6))
  )

  (symbol (lib_id "power:+2V8") (at 210.82 69.85 0) (unit 1)
    (in_bom yes) (on_board yes) (fields_autoplaced)
    (uuid a10cdecd-53db-4a94-874a-3261fddc42c8)
    (property "Reference" "#PWR01" (id 0) (at 210.82 73.66 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Value" "+2V8" (id 1) (at 210.82 64.77 0))
    (property "Footprint" "" (id 2) (at 210.82 69.85 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Datasheet" "" (id 3) (at 210.82 69.85 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (pin "1" (uuid 78de31e4-8d4c-4852-83a5-0327196b1ea4))
  )

  (symbol (lib_id "power:GND") (at 163.195 104.775 0) (unit 1)
    (in_bom yes) (on_board yes) (fields_autoplaced)
    (uuid a4b2225d-2274-43bc-a784-6b561415b8ac)
    (property "Reference" "#PWR0101" (id 0) (at 163.195 111.125 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Value" "GND" (id 1) (at 163.195 109.22 0))
    (property "Footprint" "" (id 2) (at 163.195 104.775 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Datasheet" "" (id 3) (at 163.195 104.775 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (pin "1" (uuid 0dea4a7f-6dc3-4b6d-a485-44dbd7d6f80b))
  )
)
